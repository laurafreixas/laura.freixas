void setup(){
    Serial.begin(9600);//Set Baud Rate to 9600 bps
}

 void loop()
{   unsigned int val;
    unsigned int dat;
    val=analogRead(0);//Connect LM35 on Analog 0
    dat=(500 * val) /1024;;
    Serial.print("Temp:"); //Display the temperature on Serial monitor
    Serial.print(dat);
    Serial.println("ºC");
    delay(1000);
  }
