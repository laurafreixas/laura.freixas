// These constants won't change:
const int analogPin = A0;    // pin that the sensor is attached to
const int ledPin = 13;       // pin that the LED is attached to
const int threshold = 31;   // an arbitrary threshold level that's in the range of the analog input

void setup()
{
    // initialize the LED pin as an output:
    pinMode(ledPin, OUTPUT);
    Serial.begin(9600);//Set Baud Rate to 9600 bps
}
 void loop()
{   unsigned int val;
    unsigned int dat;
    val=analogRead(analogPin);//Connect LM35 on Analog 0
    dat=(500 * val) /1024;;
    Serial.print("Temp:"); //Display the temperature on Serial monitor
    Serial.print(dat);
    Serial.println("ºC");
    delay(1000);
    
    // read the value of the potentiometer:
  //int analogValue = analogRead(analogPin);

  // if the analog value is high enough, turn on the LED:
  if (dat > threshold) {
    digitalWrite(ledPin, HIGH);
  } else {
    digitalWrite(ledPin, LOW);
  }

}
